/*Для создания класса с двумерным динамическим массивом и возможностью глубокого копирования,
будем использовать указатели и перегрузку оператора присваивания.*/

#include <iostream>
#include <ctime>
#include <iomanip>
using namespace std;

class MyMatrix
{
    //Динамический массив указателей Matrix
private:
    float **Matrix;     // указатель на двумерный динамический массив
    int Row;            // количество строк в массиве
    int Column;         // количество столбцов в массиве
    int *extraData;     // указатель на дополнительные данные типа int
    string *Info;       // указатель на дополнительные данные типа string
public:
//    MyMatrix(int R, int C) //Constructor
    MyMatrix(int R, int C) : Row(R), Column(C) //Можно и так присвоить сразу
    {
// или так или просто так: Column = C;
//        this->Column = C;
//        this->Row = R;

        // выделение памяти под двумерный динамический массив
        Matrix = new float *[Row];
        for (int i = 0; i < Row; i++) {
            Matrix[i] = new float[Column];
        }

        // выделение памяти под дополнительные данные
        extraData = new int;
        Info = new string("Hello");
    }

    // деструктор динамического массива
    ~MyMatrix() {
        // освобождение памяти, выделенной под двумерный динамический массив
        for (int i = 0; i < Row; i++)
            delete[] Matrix[i];
        delete[] Matrix;

        // освобождение памяти, выделенной под дополнительные данные
        delete extraData;
        delete Info;
    }

    // заполнение массива
    void addMyMatrix(int r, int c) { Matrix[r][c] = (rand() % 10 + 1) / float((rand() % 10 + 1));; }

    // вывод массива
    void print();

    // Конструктор копирования
    MyMatrix(const MyMatrix &other) {
        Row = other.Row;
        Column = other.Column;
        // выделение памяти под двумерный динамический массив
        Matrix = new float *[Row];
        for (int i = 0; i < Row; i++) {
            Matrix[i] = new float[Column];
        }

        // копирование значений из другого объекта
        for (int i = 0; i < Row; i++) {
            for (int j = 0; j < Column; j++) {
                Matrix[i][j] = other.Matrix[i][j];
            }

            // выделение памяти и копирование значений для дополнительных данных
            extraData = new int;
            *extraData = *(other.extraData);
            Info = new string("Hello");
            *Info = *(other.Info);
        }
    }

        // оператор присваивания
        MyMatrix &operator=(const MyMatrix &other){
        // проверяем на самоприсваивание
        if (this == &other){
            return *this;
        }

        // освобождение памяти текущих данных
        for (int i = 0; i < Row; i++){
            delete[] Matrix[i];
        }
        delete[] Matrix;
        delete extraData;
        delete Info;

        Row = other.Row;
        Column = other.Column;

            // выделение памяти под двумерный динамический массив
            Matrix = new float *[Row];
            for (int i = 0; i < Row; i++) {
                Matrix[i] = new float[Column];
            }

        // копирование значений из другого объекта
        for (int i = 0; i < Row; i++) {
            for (int j = 0; j < Column; j++) {
                Matrix[i][j] = other.Matrix[i][j];
            }
        }
            // выделение памяти и копирование значений для дополнительных данных
            extraData = new int;
            *extraData = *(other.extraData);
            Info = new string("Hello");
            *Info = *(other.Info);

            // возвращаем существующий объект, чтобы можно было включить этот оператор в цепочку
            return *this;
        }
    };

    void MyMatrix::print() {
       // cout << Row << Column;
        for (int u = 0; u < Row; u++) {
            for (int v = 0; v < Column; v++)
                cout << setw(4) << setprecision(2) << " Matrix[" << u << "][" << v << "] = " << Matrix[u][v] << " ";
            cout << endl;
        }
    }

/*Создаём объект `MTest` класса `MyMatrix` с двумерным динамическим массивом и размером Row_xCol_.
 Затем создаём объект `G` инициализированный значением `MTest`.
 При инициализации `G` используем конструктор копирования,
 который выполняет глубокое копирование двумерного массива и дополнительных данных.

Также создаём объект `A` класса `MyMatrix` и значение `MTest` присваивается `A`
 с использованием оператора присваивания. В этом случае также выполняется глубокое копирование. */

    int main() {
        setlocale(LC_ALL, "Russian");

        int Row_ = 3;
        int Col_ = 5;

        MyMatrix MTest(Row_, Col_);

        for (int count_row = 0; count_row < Row_; count_row++)
            for (int count_column = 0; count_column < Col_; count_column++) {
                MTest.addMyMatrix(count_row, count_column);
            }
        cout  <<"Martix MTest : " << endl;
        MTest.print();
        cout << endl;

        // вызов конструктора копирования
        MyMatrix G = MTest;
        cout  <<"Matrix G - Copy MTest: " << endl;
        G.print();
        cout << endl;

        // вызов оператора присваивания
        MyMatrix A(2, 5);
        A = MTest;
        cout  <<"Matrix A - deep copy Mtest: " << endl;
        A.print();

        cout << "______________________________" << endl;

        return 0;
    }